# lqdn-docker-image

## Description

This docker image is used to deploy the CI of La Quadrature du Net's Ansible playbooks.

### Update

Every so often you need to update the docker image. To do so :

```
docker build . -t registry.git.laquadrature.net/lqdn-interne/lqdn-docker-image/container
```

This will build the docker image.

```
docker login registry.git.laquadrature.net
```

Login to the registry ( you will need to Access Token, create it from your Gitlab Profile )

```
docker push registry.git.laquadrature.net/lqdn-interne/lqdn-docker-image/container:latest                                            1 ↵
```

This will push the image you just built to the `latest` tag in the repository.

## Badges

## Usage

It's supposed to be used with Gitlab Runners.

## Support

See the matrix group of La Quadrature Du Net

## Roadmap

Maybe use Alpine Linux instead of Debian as a base.

## Contributing

Yes please !

## Authors and acknowledgment

nono <nono@laquadrature.net>

## License

GPLv3
