FROM docker.io/python:3.12-bookworm

MAINTAINER Nono LQDN <nono@laquadrature.net>

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qy
RUN apt install -y curl openssh-client python3 python3-pip git tree
RUN pip install ansible ansible-lint passlib[bcrypt] jmespath molecule molecule-plugins[vagrant]

# Install/prepare Ansible
RUN mkdir -p /etc/ansible/
RUN mkdir -p /opt/ansible/roles
RUN rm -f /opt/ansible/hosts

CMD /bin/bash